# TwistedBot: Destiny Trials Counter/CSS  #

TwistedBot is a Node.js Destiny/Twitch/OBS trials counter used to update the number of wins/losses for a Trials session. Commands that can be used are as follows.

!wins - Tells your channel how many wins, losses and if Mercy is available.
!win  - Increments (currently a flat file) to the next win by 1.
!loss - Increments the loss count by one or uses Mercy if that's available.
!reset - Reset the card back to one win and assume Mercy.

### What is this repository for? ###

* Objective of this repository is to provide a reliable way for Twitch streamers to add a counter to their streaming software and be able to use any account without modifying their stream. This is NOT connected to the Bungie/Destiny API so no stats are available at this time.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact