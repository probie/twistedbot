var tmi = require('tmi.js');
var oauth = require('./oauth.js');
var fs = require('fs');
var fileName = './data/TrialsCounter.json';
var file = require(fileName);

/*******************************************
 Requires node, npm, a bot Twitch account and
 your oauth.js containing your twitch api key.

 TODO: Make this bot dynamic so we can place
 it on Heroku and sell it to other streamers.
 *******************************************/

var options = {
  options: {
    color: "Firebrick",
    debug: true
  },
  connection: {
    cluster: "aws",
    reconnect: true
  },
  identity: {
    username: "twistedbot_v1",
    password: oauth.password
  },
  channels: ['#txdev']
};


var client = new tmi.client(options);
client.connect();

client.on('chat', (channel, user, message, self) => {
  if(self) return;

  if(channel) {

    switch(message) {
      case "!wins":

          // Display the current card to the channel.
          fs.readFile("data/TrialsCounter.json", 'utf8', function (err, data) {
            if (err) {
              return console.log(err);
            } else {
              var obj = JSON.parse(data);

              if(obj.Mercy === true) {
                obj.Mercy = "Available";
              } else {
                obj.Mercy = "Used";
              }

              client.say(channel, "Current card... Wins: " + obj.Wins + " Losses: " + obj.Losses + " Mercy: " + obj.Mercy);
            }
          });

        break;

      case "!win":

        // Increment the number of wins
        if(user.mod) {
          fs.readFile('data/TrialsCounter.json', 'utf8', function(err, data){
            if(err) {
              return console.log(err);
            }
            var obj = JSON.parse(data);

            console.log("Token " + obj.Wins );
            client.say(channel, "Congrats on the win! Let's gooooooo!");

            obj.Wins++;

            fs.writeFile('data/TrialsCounter.json', JSON.stringify(obj), function(err){
              if(err){
                return console.log(err);
              } else {
                console.log("Conplete!!");
              }
            });

          });
        }
        break;

      case "!loss":
        // Get current state
        //var filenName = "data/TrialsCounter.json";
        if(user.mod) {
          fs.readFile('data/TrialsCounter.json', 'utf8', function(err, data){
            if(err) {
              return console.log(err);
            }
            var obj = JSON.parse(data);

              if(obj.Mercy) { // If TRUE
                console.log("Token " + obj.Mercy );
                obj.Mercy = false;
              } else {
                if(obj.Losses <= 2) {
                  obj.Losses++;
                } else {
                  client.say(channel, "Head to the Reef for a new card! Oh yeah and type !reset too.");
                }
              }
              //console.log("Token " + obj.Mercy );
              //obj.push = obj.Mercy;

            fs.writeFile('data/TrialsCounter.json', JSON.stringify(obj), function(err){
              if(err){
                return console.log(err);
              } else {
                console.log("Conplete!!");
              }
            });

          });
        }
        break;

      case "!reset":
        // Reset the current card [COMPLETED]
        if(user.mod) {
          var resetCount = {
            "Wins": 1,
            "Losses": 0,
            "Mercy": true
          }; // Assume Mercy

          var tc = JSON.stringify(resetCount);
          // Write to our file to be use in OBS
          fs.writeFile('data/TrialsCounter.json', tc, function (err) {
            if (err) {
              client.say("#txdev", "Error: " + err);
              return console.log(err);
            }
          });

          obj = JSON.parse(tc);
          client.say(channel, "Card is reset with Wins: " + obj.Wins + " Losses: " + obj.Losses + " Mercy: " + obj.Mercy);
        }
        break;

      default:
        break;
    }
  }

});

client.on('connected', function(address, port){
  //client.action("#txdev", "Wonder Twin Powers... Activate!");
  console.log("Now working for: " + options.channels);
});
